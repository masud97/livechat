<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chats', function (Blueprint $table) {
            $table->increments('id');            
            $table->integer('sender_user_id')->unsigned()->index();
            $table->integer('receiver_user_id')->unsigned()->index();
            $table->text('message');
            $table->tinyInteger('is_seen')->default(1);
            $table->tinyInteger('is_user_seen')->default(1);
            $table->tinyInteger('typing')->nullable();
            $table->timestamps();

            $table->foreign('sender_user_id')->references('id')->on('users');
            $table->foreign('receiver_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chats');
    }
}
