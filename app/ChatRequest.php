<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Chat;

class ChatRequest extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    protected $table = 'chat_requests';

    public function sender_user(){
        return $this->belongsTo(User::class, 'sender_user_id');
    }

    public function receiver_user(){
        return $this->belongsTo(User::class, 'receiver_user_id');
    }

    public function chats(){
        return $this->hasMany(Chat::class);
    }

}
