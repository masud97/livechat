<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ChatRequest;

class Chat extends Model
{
    //

    public function chat_request(){
        return $this->belongsTo(ChatRequest::class);
    }
}
