<?php

namespace App\Http\Controllers;
use App\Chat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\ChatRequest;
use App\User;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    { 
        return view('home');
    }

    public function allmessage()
    { 
        $id = Auth::id();
        User::where('id', $id)->update(['login_status' => 1]);
        return view('allmessage');
    }

    public function showUsers(){
        $userId = Auth::id();

        $users = ChatRequest::where('sender_user_id',$userId)
        ->where('status', 1) 
        ->orWhere('receiver_user_id', $userId)
        ->where('status', 1)
        ->with('sender_user', 'receiver_user')
        ->get();      

        /* echo "<pre>";
        print_r($users);
        die(); */

        return view('users', ['users' => $users]);
    }

    function jsonResponse(){
        $user = DB::table('chats')->get();
        return response()->json($user);
    }
}
